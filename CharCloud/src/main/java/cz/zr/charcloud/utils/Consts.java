/*
 * Released under the MIT/X11 License.
 *
 * Copyright (c) 2017 Zbynek Ruzicka
 *
 */

package cz.zr.charcloud.utils;

/**
 * Constants
 * 
 * @author ZRuzicka
 */
public class Consts {

    /** Processed content encoding. */
    public static String ENCODING = "UTF-8";

    /** Character minimal guaranteed size. */
    public static int CHAR_MINIMAL_SIZE = 10;

    /** Additional size added based on character occurrence ratio. */
    public static int CHAR_ADDITIONAL_SIZE_RANGE = 20;

}
