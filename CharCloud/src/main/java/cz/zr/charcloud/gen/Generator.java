/*
 * Released under the MIT/X11 License.
 *
 * Copyright (c) 2017 Zbynek Ruzicka
 *
 * You should have received a copy of the License along with this project in the project root.
 */

package cz.zr.charcloud.gen;


/**
 * Common Generator interface.
 * 
 * @author ZRuzicka
 */
public interface Generator {
    // Marker interface.

    // TODO Common Generator methods would be worth. 
}
